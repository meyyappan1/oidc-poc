/*
This is an example application to demonstrate parsing an ID Token.
*/
package main

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
)

var (
	clientID     = "781729ab-87b2-4116-9730-018d3a87340a"
	clientSecret = "Ic7UmbAql._nAJp~159~-OPr3Af.1vw22d"
)

func randString(nByte int) (string, error) {
	b := make([]byte, nByte)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(b), nil
}

func setCallbackCookie(w http.ResponseWriter, r *http.Request, name, value string) {
	c := &http.Cookie{
		Name:     name,
		Value:    value,
		MaxAge:   int(2 * time.Hour.Seconds()),
		Secure:   r.TLS != nil,
		HttpOnly: true,
	}
	if name == "mysessiontoken" {
		c.Path = "/"
	}
	http.SetCookie(w, c)
}

func main() {
	ctx := context.Background()
	kvStore := make(map[string]interface{})
	provider, err := oidc.NewProvider(ctx, "https://login.microsoftonline.com/dd3dfd2f-6a3b-40d1-9be0-bf8327d81c50/v2.0")
	if err != nil {
		log.Fatal(err)
	}
	oidcConfig := &oidc.Config{
		ClientID: clientID,
	}
	verifier := provider.Verifier(oidcConfig)

	config := oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Endpoint:     provider.Endpoint(),
		RedirectURL:  "https://54.212.107.157:5556/auth/google/callback",
		Scopes:       []string{oidc.ScopeOpenID, oidc.ScopeOfflineAccess, "profile", "email", "User.Read"},
	}

	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		state, err := randString(16)
		if err != nil {
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
		nonce, err := randString(16)
		if err != nil {
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
		setCallbackCookie(w, r, "state", state)
		setCallbackCookie(w, r, "nonce", nonce)

		http.Redirect(w, r, config.AuthCodeURL(state, oidc.Nonce(nonce)), http.StatusFound)
	})

	http.HandleFunc("/auth/google/callback", func(w http.ResponseWriter, r *http.Request) {
		state, err := r.Cookie("state")
		if err != nil {
			http.Error(w, "state not found", http.StatusBadRequest)
			return
		}
		if r.URL.Query().Get("state") != state.Value {
			http.Error(w, "state did not match", http.StatusBadRequest)
			return
		}

		oauth2Token, err := config.Exchange(ctx, r.URL.Query().Get("code"))
		if err != nil {
			http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
			return
		}
		rawIDToken, ok := oauth2Token.Extra("id_token").(string)
		if !ok {
			http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
			return
		}
		rawRefreshToken, ok := oauth2Token.Extra("refresh_token").(string)
		if !ok {
			http.Error(w, "No refresh_token field in oauth2 token.", http.StatusInternalServerError)
			return
		}
		idToken, err := verifier.Verify(ctx, rawIDToken)
		if err != nil {
			http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
			return
		}

		nonce, err := r.Cookie("nonce")
		if err != nil {
			http.Error(w, "nonce not found", http.StatusBadRequest)
			return
		}
		fmt.Println(idToken.Nonce)
		if idToken.Nonce != nonce.Value {
			http.Error(w, "nonce did not match", http.StatusBadRequest)
			return
		}

		//oauth2Token.AccessToken = "*REDACTED*"
		sessionToken, err := randString(16)
		if err != nil {
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
		kvStore[sessionToken] = map[string]string{
			"idToken":      rawIDToken,
			"refreshToken": rawRefreshToken,
		}
		fmt.Println("------New Login session----------")
		fmt.Println(sessionToken)
		fmt.Println("")
		fmt.Println("token:" + rawIDToken)
		fmt.Println("")
		fmt.Println("refresh token:" + rawRefreshToken)
		fmt.Println("---------------------------------")
		fmt.Println("List of valid controller sessions")
		for k := range kvStore {
			fmt.Println(k)
		}

		resp := struct {
			OAuth2Token   *oauth2.Token
			IDTokenClaims *json.RawMessage // ID Token payload is just JSON.
		}{oauth2Token, new(json.RawMessage)}

		if err := idToken.Claims(&resp.IDTokenClaims); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		data, err := json.MarshalIndent(resp, "", "    ")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		setCallbackCookie(w, r, "mysessiontoken", sessionToken)
		w.Write(data)
	})

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		sessionToken, err := r.Cookie("mysessiontoken")
		if err != nil {
			http.Error(w, "mysessiontoken not found", http.StatusBadRequest)
			return
		}
		var tokens interface{}
		var ok bool
		if tokens, ok = kvStore[sessionToken.Value]; !ok {
			http.Error(w, "you are not logged in", http.StatusUnauthorized)
			return
		}
		v, ok := tokens.(map[string]string)
		if !ok {
			http.Error(w, "interface conversion problem", http.StatusBadRequest)
			return
		}
		idToken, err := verifier.Verify(ctx, v["idToken"])
		if err != nil {
			if !strings.Contains(err.Error(), "token is expired") {
				http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
				return
			}
			//token has expired, so refresh token
			fmt.Println("-----Requesting new token--------")
			ts := config.TokenSource(ctx, &oauth2.Token{RefreshToken: v["refreshToken"]})
			tok, err := ts.Token()
			rawIDToken, ok := tok.Extra("id_token").(string)
			if !ok {
				http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
				return
			}
			rawRefreshToken, ok := tok.Extra("refresh_token").(string)
			if !ok {
				http.Error(w, "No refresh_token field in oauth2 token.", http.StatusInternalServerError)
				return
			}
			fmt.Println("")
			fmt.Println("new token:" + rawIDToken)
			fmt.Println("")
			fmt.Println("new refresh token:" + rawRefreshToken)
			fmt.Println("")
			fmt.Println("---------------------------------")
			idToken, err = verifier.Verify(ctx, rawIDToken)
			if err != nil {
				http.Error(w, "Failed to verify the NEW ID Token: "+err.Error(), http.StatusInternalServerError)
				return
			}
			//store the new token in the kv store
			kvStore[sessionToken.Value] = map[string]string{
				"idToken":      rawIDToken,
				"refreshToken": rawRefreshToken,
			}
			fmt.Println("List of valid controller sessions")
			for k := range kvStore {
				fmt.Println(k)
			}
		}
		nonce, err := r.Cookie("nonce")
		if err != nil {
			http.Error(w, "nonce not found", http.StatusBadRequest)
			return
		}
		if idToken.Nonce != nonce.Value {
			http.Error(w, "nonce did not match", http.StatusBadRequest)
			return
		}
		w.Write([]byte("hello world!!"))

	})

	http.HandleFunc("/logout", func(w http.ResponseWriter, r *http.Request) {
		sessionToken, err := r.Cookie("mysessiontoken")
		if err != nil {
			http.Error(w, "mysessiontoken not found", http.StatusBadRequest)
			return
		}
		var tokens interface{}
		var ok bool
		if tokens, ok = kvStore[sessionToken.Value]; !ok {
			w.Write([]byte("You have been logged out"))
			return
		}
		v, ok := tokens.(map[string]string)
		if !ok {
			http.Error(w, "interface conversion problem", http.StatusInternalServerError)
			return
		}
		idToken, err := verifier.Verify(ctx, v["idToken"])
		if err != nil {
			// do we need to do this??
			fmt.Println("Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
			return
		}
		nonce, err := r.Cookie("nonce")
		if err != nil {
			http.Error(w, "nonce not found", http.StatusBadRequest)
			return
		}
		if idToken.Nonce != nonce.Value {
			http.Error(w, "nonce did not match", http.StatusBadRequest)
			return
		}
		delete(kvStore, sessionToken.Value)
		fmt.Println("Removed session:" + sessionToken.Value)
		w.Write([]byte("You have been logged out"))

	})

	//log.Printf("listening on https://%s/", "127.0.0.1:5556")
	//log.Fatal(http.ListenAndServeTLS("127.0.0.1:5556", "cert.pem", "key.pem", nil))
	log.Printf("listening on https://%s/", "54.212.107.157:5556")
	log.Fatal(http.ListenAndServeTLS(":5556", "cert.pem", "key.pem", nil))
}
